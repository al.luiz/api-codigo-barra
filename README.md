# API Validação de Código de Barra

## Descrição

- API para utilização em sistemas que necessitem verificar a autenticidade de um código de barra.


## Run

- O seguinte comando executa a aplicação em modo desenvolvimento

`npm run dev`

## Endpoints

### `GET /api/codigo_barra/:id/validate`

API de validação de código de barra

Exemplo de Resposta:

```
{
    status: true
}
```


### `GET /api/codigo_barra/:id/info`

API de informação sobre o código de barra.

Exemplo de Resposta:

```
{
    status: true,
    type: 'GTIN/EAN' or 'UPC'
    length: 13
}
```

## HTTP Status

```
200: Ok. Código de barra é válido
422: Erro de negócio. Código de barra inválido
500: Erro de sistema.
```


## Routes

- Contém as rotas que serão utilizadas pela API. Alterar em caso de necessidade de inclusão de novo domínio para a rota.

**main_route.ts**: Arquivo para mapeamento global de domínios de rotas

**/api/codigo_barra.ts**: Arquivo de gerenciamento das rotas do domínio para o devido metódo do controller. Alterar em caso de necessidade de inclusão de nova rota (PUT, POST, ETC).

## Controllers

- Contém a lógica de entrada das requisições e de resposta ao cliente.

**CodBarraController.ts**: Arquivo de gerenciamento das requisições por método de acordo com a rota mapeada no item anterior. Alterar em caso de inclusão de nova rota e/ou alteração de lógica na requisição/resposta HTTP de rota existente.

## Services

- Contém a lógica de negócio do sistema.

**CodBarraService.ts**: Arquivo com a lógica de validação e processamento dos dados da requisição. Alterar em caso de inclusão de nova funcionalidade de negócio e/ou alteração da funcionalidade existente.