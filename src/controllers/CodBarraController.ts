import { Request, Response } from 'express';

import CodBarraService from '../services/CodBarraService';

const service = new CodBarraService();

class CodBarraController {

    async validate(request: Request, response: Response) {

        try {

            const cod_barra: string = request.params.id;

            const result = await service.validate(cod_barra);

            if (result === true)
                return response.json({ status: true });
            else
                return response
                    .status(422)
                    .json({ status: false });

        } catch (error) {

            return response
                .status(500)
                .json({ message: `Erro durante a validação do código de barra: ${error.message}` });

        }

    }

    async info(request: Request, response: Response) {

        try {

            const cod_barra: string = request.params.id;

            const info = await service.info(cod_barra);

            if (info.status === true)
                return response.json(info);
            else
                return response
                    .status(422)
                    .json({ status: false });

        } catch (error) {

            return response
                .status(500)
                .json({ message: `Erro durante a obtenção de informações do código de barra: ${error.message}` });

        }
    }

}

export default CodBarraController;