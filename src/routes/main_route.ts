import express from 'express';

import codBarraRouter from './api/codigo_barra';

const router =  express.Router();

router.use('/api/cod_barra', codBarraRouter);

// index => lista
// show => unico item
// create => criar
// update => atualizar
// delete or destroy => excluir

export default router;