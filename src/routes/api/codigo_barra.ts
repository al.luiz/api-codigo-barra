import express from 'express';
import CodBarraController from '../../controllers/CodBarraController';

const codBarraController = new CodBarraController();

const router =  express.Router();

router.get('/:id/validate', codBarraController.validate);
router.get('/:id/info', codBarraController.info);

export default router;