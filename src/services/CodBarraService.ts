
interface CodBarra {
    type?: string,
    status: boolean,
    length?: number
}

class CodBarraService {

    async processGTIN13(cod_barra: string) {

        let soma: number = 0;

        for (let i = 0; i < cod_barra.length - 1; i++) {
            let num: number = parseInt(cod_barra[i]);

            if (i % 2 == 1) {
                num *= 3;
            }

            soma += num;
        }

        return soma;
    }

    async processGeneral(cod_barra: string) {

        let soma: number = 0;

        for (let i = 0; i < cod_barra.length - 1; i++) {
            let num: number = parseInt(cod_barra[i]);

            if (i % 2 == 0) {
                num *= 3;
            }

            soma += num;
        }

        return soma;
    }

    async checkSum(somaProcessada: number) {

        let base10: number = somaProcessada;
        let resultado: number = 0;

        if (!(base10 % 10 == 0)) {
            while (!(base10 % 10 == 0)) {
                base10++;
            }

            resultado = base10 - somaProcessada;
        }

        return resultado;
    }

    async validate(cod_barra: string) {

        let codBarraValido: boolean = false;

        if (parseInt(cod_barra)) {

            if (cod_barra.length == 8 || cod_barra.length == 12 || cod_barra.length == 13 || cod_barra.length == 14) {

                let somaProcessada: number = 0;

                if (cod_barra.length == 13)
                    somaProcessada = await this.processGTIN13(cod_barra);
                else
                    somaProcessada = await this.processGeneral(cod_barra);

                const somaVerificada = await this.checkSum(somaProcessada);

                const lastIndex = cod_barra.length - 1;
                codBarraValido = somaVerificada == parseInt(cod_barra[lastIndex]);
            }
        }
        else {
            throw new TypeError('Necessario código de barras apenas com números.');
        }

        return codBarraValido;
    }

    async info(cod_barra: string) {

        const cod_barra_info: CodBarra = { status: false };

        const codigoValido = await this.validate(cod_barra);
        
        if (codigoValido) {

            cod_barra_info.status = true;
            cod_barra_info.length = cod_barra.length;

            if (cod_barra.length == 8) {
                cod_barra_info.type = 'UPC';
            }
            else {
                cod_barra_info.type = 'EAN/GTIN';
            }
        }

        return cod_barra_info;

    }

}

export default CodBarraService;