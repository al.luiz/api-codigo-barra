import express from 'express';
import router from './routes/main_route';
import cors from 'cors';

const app = express();

app.use(cors());
app.use(express.json());
app.use(router);

app.listen(3333);